# CarCar

Team:

* Cameron Webbe
  - Sales
  - Inventory
    - List manufacturers
    - Create manufacturers
    - List auto inventory
* Matt Roth
  - Services
  - Inventory
    - List vehicle models
    - Create vehicle models
    - Add auto to auto inventory

## Design

Link to Excalidraw Diagram: https://excalidraw.com/#json=Lyf_IkBdGTEBJoh-zrBXN,VQmcG-zE-fVKuBzah0v6_A

## Service microservice

Explain your models and integration with the inventory
microservice, here.

- **Technician**: Stores technician's name and employee number
- **AutomobileVO**: Stores all automobiles that were _ever_ in Inventory as value objects, including automobiles that have been sold. Unlike the Automobile model in the Inventory microservice, AutomobileVO is only concerned with storing its VIN and reference href.
  - This is an integration point with the Inventory microservice. The Service microservice needs this information to determine a Service Appointment's VIP status.
- **ServiceAppointment**: Stores VIN, customer, date/time, assigned technician, reason, and completed and VIP statuses for an individual service appointment instance.
  - VIP status is determined during creation (`service/api/service_rest/api_views.py:105`).
    - The appointment's associated VIN is used as the query term to retrieve a matching object from AutomobileVO (`AutomobileVO.objects.get(vin=vin)`) and store it in a variable `vin_in_inventory`.
    - If nothing is returned, a try/except will set `vin_in_inventory` as `None`.
    - Finally, an if/else statement is used to create a new key `isVIP` on the incoming content and set it to `True` or `False`.
  - Once created a Service Appointment can only update its `isCompleted` field, which determines if it will display in the list of all services, or in the VIN's completed service history.


## Sales microservice

* Models
  * AutomobilesVO gets the data from inventory-api from my poller so I can use it in my salesrecord model.
  * Salesperson stores the data for all the Salespeople
  * PotentialCustomer stores the data for all my potential customers
  * Salesrecords stores the data for sales price, and references in foriegn keys my Salespersons model, PotentialCustomer model, and the AutomobileVO model
* Views
  * Encoders
    * AutomobilesVOEncoder takes the info from my AutomobilesVO and encodes the vin, the import_href, and the id that Django automatically gives so I can later use it in my Insomnia and React Components
    * In my SalesRecordEncoder I needed the auto VIN in my record, so I used my AutomobileVOEncoder to handle just the automobile property
  * View Functions
    * In my list sales records, I had to grab the content from the json file and made it so whenever I called the automobile vin, the salespersons employee number, or the customers phone, it would then reference the corresponding object. I have a few try-except clauses that catch when the automobile, salesperson, or customer does not exist
* Poller
  * This just looks up the inventory-api container, unloads it in a var called content, and then for each item in "autos", it populates that data inside AutomobilesVO with the corrosponding variables in AutomobileVO
* React Components
  * Sales Records List: It fetches the json file from salesrecords api and once it got all that data, puts it in setState so that React can use it. Then in the dynamic html, it loops over every instance of that array and displays the information that is called
  * Sales Records History: It first creates a function that will either show select a salesperson when no search attempts have been made, or will show that the salesperson has no sales if a search has been made but no sales. It fetches all the data from salesrecords api and salespersons api and stores it in arrays. Then in the html, it creates a drop down of all the salespersons by name, and when the user selects the name of a salesperson, it filters through all the data and only displays data that the filter returns.


## Planning Roadmap

- [x] Docker setup
- [x] Design
  - [x] List Bounded Contexts
  - [x] List Value Objects and Entities
  - [x] List Agregates and Agregate Roots
- [x] Inventory Microservice
  - [x] Set up Insomnia testing endpoints
  - [x] React Components:
    - [x] List of manufacturers [Cameron]
    - [x] Create manufacturers [Cameron]
    - [x] List vehicle models [Matt]
    - [x] Create vehicle models [Matt]
    - [x] List auto inventory [Cameron]
    - [x] Add auto to auto inventory [Matt]
- [x] Begin Individual Microservice Work
- [ ] Mod 2 Week 6 Goals
  - [x] Nav dropdowns
  - [ ] Add Update Inventory Automobile
  - [ ] Add API that gets average vehicle market value, then link up that information with my a model and then have a percentage bar on each sales record for how much above or below market value that vehicle sold for [Cameron]

- [x] Services
  - [x] RESTful API
    - [x] Technician 
      - [x] List
      - [x] Create
      - [x] Detail
    - [x] Service Appointment
      - [x] Create
      - [x] Detail
      - [x] List
      - [x] Delete
      - [x] Update (toggle isComplete)
  - [x] Poller
    - [x] Continuously poll Inventory (api_automobiles) for AutomobileVO
  - [x] React components
    - [x] Enter technician (form)
      - [x] Name
      - [x] Employee Number
    - [x] Enter service appointment (form)
      - [x] VIN
      - [x] Customer Name
      - [x] Date/time of appointment
      - [x] Reason for appointment
      - [x] Assigned technician
    - [x] List of appointments (table)
      - [x] VIN
      - [x] Customer Name
      - [x] Date/time of appointment
      - [x] Assigned technician
      - [x] Reason for appointment
      - [x] FEATURE: If VIN was ever in Inventory (came from dealership), give VIP
      - [x] FEATURE: Include cancel (delete) and complete (isCompleted toggle) buttons
    - [x] Show history for specific VIN
      - [x] Ideally only show appointments where isCompleted == True

* - [x] Sales
  * - [x] RESTful API
  * - [x] Poller
  * - [x] React components
    * - [x] Add Salesperson Form
    * - [x] Add Potential Customer Form
    * - [x] Create a Sale Record
      * - [x] Automobile
      * - [x] Salesperson
      * - [x] Customer
      * - [x] Sale Price
      * - [x] FEATURE Add button/link in Nav bar to create a new record
    * - [x] List All Sales (Table)
      * - [x] Salesperson's Name
      * - [x] Employee Number
      * - [x] Purchaser's Name
      * - [x] VIN
      * - [x] Price of Sale
      * - [x] FEATURE Give a link in the Nav Bar for Sales List
      * - [x] FEATURE When form is submitted, the sales record is stored in the app
    * - [x] List a Salesperson's History
      * - [x] FEATURE Create a page with a dropdown that allows to choose a sales person from it. When someone chooses, fetch list of salesperson's history
      * - [x] Salesperson's Name
      * - [x] Customer
      * - [x] VIN
      * - [x] Sale Price
      * - [x] FEATURE Create a button/link in the nav bar to the page
