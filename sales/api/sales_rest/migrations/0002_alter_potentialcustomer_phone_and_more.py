# Generated by Django 4.0.3 on 2022-08-02 23:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='potentialcustomer',
            name='phone',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='salesperson',
            name='employee_number',
            field=models.IntegerField(unique=True),
        ),
    ]
