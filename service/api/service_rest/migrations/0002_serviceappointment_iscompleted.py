# Generated by Django 4.0.3 on 2022-08-02 22:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceappointment',
            name='isCompleted',
            field=models.BooleanField(default=False),
        ),
    ]
