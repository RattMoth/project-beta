# Generated by Django 4.0.3 on 2022-08-02 21:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Technician',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('employee_number', models.PositiveSmallIntegerField(unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='ServiceAppointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.PositiveSmallIntegerField(unique=True)),
                ('customer_name', models.CharField(max_length=100)),
                ('reason', models.CharField(max_length=500)),
                ('assigned_technician', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assigned_technician', to='service_rest.technician')),
            ],
        ),
    ]
